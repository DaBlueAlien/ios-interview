# README #

iOS Interview

### Problems to solve ###

* GreetingsView does not display "Hello, Sameh!", fix that in AppDelegate.

* Image displays when users launch the app for the first time only, launching the app again won't display the image, fix that in ImageViewController.

* Layout issue when greenView is removed, fix that in Main.storyboard using only constraints.
* Add animation when greenView issue gets fixed, do that in LayoutViewController.
### Or ###
* Animate orange and purple views as shown in attached video, using constraints.

* APIViewController displays a label with text "Users Count:", it should display "Users Count: 10", 10 is an example, real users count may be different.

* Selecting a student and changing his/her name in StudentDetailsViewController affects his/her name in StudentsViewController. Explain what that happens, and prevent that from happening.
