//
//  Company.swift
//  Interview
//
//  Created by Sameh Salama on 14/12/2020.
//

import Foundation

struct Company: Codable {
    var name:String!
    var catchPhrase:String!
    var bs:String!
}
