//
//  Student.swift
//  Interview
//
//  Created by Sameh Salama on 17/05/2021.
//

import Foundation

class Student {
    var name:String
    
    init(name:String) {
        self.name = name
    }
}
