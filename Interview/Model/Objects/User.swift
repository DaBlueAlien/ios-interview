//
//  User.swift
//  Interview
//
//  Created by Sameh Salama on 14/12/2020.
//

import Foundation

struct User: Codable {
    var id:Int!
    var name:String!
    var username:String!
    var email:String!
    var address: Address!
    var phone:String!
    var website:String!
    var company:Company!
}
