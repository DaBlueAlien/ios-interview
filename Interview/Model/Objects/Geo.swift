//
//  Geo.swift
//  Interview
//
//  Created by Sameh Salama on 14/12/2020.
//

import Foundation

struct Geo: Codable {
    var lat:Double!
    var lng:Double!
}
