//
//  Address.swift
//  Interview
//
//  Created by Sameh Salama on 14/12/2020.
//

import Foundation

struct Address: Codable {
    var street:String!
    var suite:String!
    var city:String!
    var zipcode:String!
    var geo:Geo!
}
