//
//  Networking.swift
//  Interview
//
//  Created by Sameh Salama on 14/12/2020.
//

import Foundation

class Networking {
    
    static func getUsers(completion:@escaping ([User]?, Error?) -> Void) {
        guard let url = URL(string: "https://jsonplaceholder.typicode.com/users") else {return}
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                completion(nil, error)
                return
            }
            guard let data = data, let users = try? JSONDecoder().decode([User].self, from: data) else {
                completion(nil, nil)
                return
            }
            completion(users, nil)
        }
        task.resume()
    }
    
}
