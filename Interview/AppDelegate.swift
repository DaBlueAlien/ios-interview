//
//  AppDelegate.swift
//  Interview
//
//  Created by Sameh Salama on 09/12/2020.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let greetingsVC = mainStoryboard.instantiateViewController(withIdentifier: "greetings scene") as! GreetingsViewController
        greetingsVC.name = "Sameh"
        
        return true
    }

    


}

