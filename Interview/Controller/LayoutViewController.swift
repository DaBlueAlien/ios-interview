//
//  LayoutViewController.swift
//  Interview
//
//  Created by Sameh Salama on 09/12/2020.
//

import UIKit

class LayoutViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var greenView: UIView!
    
    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func removeGreenView(_ sender: UIButton) {
        if greenView != nil {
            greenView.removeFromSuperview()
        }
    }
    
}
