//
//  StudentDetailsViewController.swift
//  Interview
//
//  Created by Sameh Salama on 17/05/2021.
//

import UIKit

class StudentDetailsViewController: UIViewController {

    @IBOutlet weak var studentNameTextField: UITextField!
    
    var student: Student!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        studentNameTextField.text = student?.name
        
//        if let student = student {
//            studentNameTextField.text = student.name
//        }
        
    }
    

    @IBAction func studentNameTextFieldDidChangeEditing(_ sender: UITextField) {
        guard let studentName = sender.text else {return}
        guard !studentName.isEmpty else {return}
        student.name = studentName
    }
    

}



