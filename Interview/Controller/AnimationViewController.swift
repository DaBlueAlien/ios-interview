//
//  LayoutTwoViewController.swift
//  Interview
//
//  Created by Sameh Salama on 20/12/2020.
//

import UIKit

class LayoutTwoViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var orangeViewVCenterConstraint: NSLayoutConstraint!
    @IBOutlet weak var orangeViewHCenterConstraint: NSLayoutConstraint!
    @IBOutlet weak var purpleViewVCenterConstraint: NSLayoutConstraint!
    @IBOutlet weak var purpleViewHCenterConstraint: NSLayoutConstraint!
    
    let animationDuration: Double = 0.3
    let verticalOffsetValue: CGFloat = 120
    let horizontalOffsetValue: CGFloat = 60
    var didInterchangePositions: Bool = false
    var didEndAnimating: Bool = true
    
    //MARK: - IBActions
    @IBAction func animate(_ sender: UIButton) {
        if didEndAnimating {
            didEndAnimating = false
            animate { doneAnimating in
                if doneAnimating {
                    self.didInterchangePositions.toggle()
                    self.didEndAnimating = true   
                }
            }
        }
        
    }
    
    private func animate(completion: @escaping (Bool) -> Void) {
        orangeViewHCenterConstraint.constant -= didInterchangePositions ? horizontalOffsetValue : -horizontalOffsetValue
        purpleViewHCenterConstraint.constant += didInterchangePositions ? horizontalOffsetValue : -horizontalOffsetValue
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        } completion: { (done) in
            if done {
                self.orangeViewVCenterConstraint.constant += self.didInterchangePositions ? -self.verticalOffsetValue : self.verticalOffsetValue
                self.purpleViewVCenterConstraint.constant -= self.didInterchangePositions ? -self.verticalOffsetValue : self.verticalOffsetValue
                UIView.animate(withDuration: self.animationDuration) {
                    self.view.layoutIfNeeded()
                } completion: { done in
                    if done {
                        self.orangeViewHCenterConstraint.constant += self.didInterchangePositions ? self.horizontalOffsetValue : -self.horizontalOffsetValue
                        self.purpleViewHCenterConstraint.constant -= self.didInterchangePositions ? self.horizontalOffsetValue : -self.horizontalOffsetValue
                        UIView.animate(withDuration: self.animationDuration) {
                            self.view.layoutIfNeeded()
                        } completion: { done in
                            if done {
                                completion(true)
                            }
                        }
                    }
                }

            }
        }
    }

}
