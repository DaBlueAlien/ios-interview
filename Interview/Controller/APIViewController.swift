//
//  APIViewController.swift
//  Interview
//
//  Created by Sameh Salama on 10/12/2020.
//

import UIKit

class APIViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var usersCountLabel:UILabel!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        getUsers()
    }
    
    private func getUsers() {
        activityIndicatorView.startAnimating()
        Networking.getUsers { (users, error)  in
            DispatchQueue.main.async {
                self.activityIndicatorView.stopAnimating()
            }
            if let error = error {
                print(error)
                return
            }
            guard let users = users else {return}
            DispatchQueue.main.async {
                self.usersCountLabel.text?.append("\(users.count)")
            }
        }
    }

}









