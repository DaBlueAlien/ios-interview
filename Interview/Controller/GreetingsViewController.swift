//
//  ViewController.swift
//  Interview
//
//  Created by Sameh Salama on 09/12/2020.
//

import UIKit

class GreetingsViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var greetingLabel: UILabel!
    
    //MARK: - Properties
    var name:String?
    
    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if let name = name {
            greetingLabel.text = "Hello, " + name + "!"
        }
    }


}

