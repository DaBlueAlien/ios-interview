//
//  ImageViewController.swift
//  Interview
//
//  Created by Sameh Salama on 09/12/2020.
//

import UIKit

class ImageViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var iconImageView: UIImageView!
    
    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        loadImage()
    }
    
    //MARK: - Custom Functions
    func loadImage() {
        guard let iconBundleUrl = Bundle.main.url(forResource: "development", withExtension: "png") else {return}
        let fileManager = FileManager.default
        let documentDirectoryUrl = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
        let iconDocumentUrl = documentDirectoryUrl.appendingPathComponent("development.png")
        do {
            try fileManager.copyItem(at: iconBundleUrl, to: iconDocumentUrl)
            let image = UIImage(contentsOfFile: iconDocumentUrl.path)
            iconImageView.image = image
        }
        catch {
            print("File Manager Error")
        }
    }

}
