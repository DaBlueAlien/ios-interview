//
//  StudentsViewController.swift
//  Interview
//
//  Created by Sameh Salama on 17/05/2021.
//

import UIKit

class StudentsViewController: UIViewController {

    @IBOutlet weak var studentsTableView: UITableView!
    
    private var students: [Student] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        students = generateStudents()
        
        studentsTableView.register(UITableViewCell.self, forCellReuseIdentifier: "student cell")
        studentsTableView.dataSource = self
        studentsTableView.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        studentsTableView.reloadData()
    }
    private func generateStudents() -> [Student] {
        var students: [Student] = []
        for i in 0...3 {
            let student = Student(name: "Student \(i)")
            students.append(student)
        }
        return students
    }


}


extension StudentsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        students.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "student cell")!
        cell.textLabel?.text = students[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let studentDetailsVC = storyboard?.instantiateViewController(withIdentifier: "studentDetailsVC") as? StudentDetailsViewController else {return}
        
        studentDetailsVC.student = students[indexPath.row]
        navigationController?.pushViewController(studentDetailsVC, animated: true)
    }
    
}
